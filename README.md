# This is a test for CSS skills 
##In this Exercise you are going tot use different types of selectors to style the elements.
###1. Link the CSS file into your HTML
###2. Grab the [Body] element and add a background color using a HEX Code.
###3. Grab the [h1] element and center the text, make the text RED and add a text-shadow.
###4. Grab the [P] tag and color the text using a Color Name.
###5. Grab the [h2] tag and add a class to it. Using the class, add a text color to [h2]
###6. Add an ID to the [h3]. Using the ID give it a text color, background color and border.
